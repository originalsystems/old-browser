# Outdated browser #

Sources:
- http://outdatedbrowser.com/ru *(old)*
- https://bestvpn.org/outdatedbrowser/ru *(new)*

Old command for clone site is:
```bash
wget -r -k -l 7 -p -E -nc http://outdatedbrowser.com/ru
```

Current command clone site content into outdatedbrowser.com, 
need to small cleanup after download html and scripts.

New method: just press ctrl+s on page and manual cleanup code,
because source site add browser cloudflare protection.
